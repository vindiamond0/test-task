import Vue from "vue";
import VueRouter from "vue-router";
import SocketIO from "socket.io-client";
import VueSocketIOExt from "vue-socket.io-extended";
import HighchartsVue from "highcharts-vue";

Vue.use(VueRouter);
Vue.use(HighchartsVue);

const socket = SocketIO('http://localhost:3000');

Vue.use(VueSocketIOExt, socket);

const routes = [
  {
    path: "/chart",
    name: "chart",
    component: () => import("../views/chart.vue"),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/",
    name: "login",
    component: () => import("../views/login.vue")
  },
  {
    path: "/register",
    name: "register",
    component: () => import("../views/register.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem("jwt") == null) {
      next({
        path: "/"
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
