# Test task for NodeJS and Vue.JS

## Installation 
- clone the repo
- execute `cp /server/example.env /server/.env` and replace with proper settings/credentials
- run the command `yarn install` in both `/server` and `/client` folders
- run apps using `yarn serve` command (same in both directories)

## Persistence
- setup MongoDB and provide the host and credentials in `/server/.env` file
- create db test with collection called charts
- import into MongoDB the charts collection from `/server/mongodb-data.json` file

## Chart page screenshot
![chart](screen.png)