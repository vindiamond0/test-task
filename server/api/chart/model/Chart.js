const mongoose = require("mongoose");
const chartSchema = mongoose.Schema({
  name: {
    type: String,
    required: [true, "Name is required"],
    unique: true
  },
  rate: {
    type: Number,
    required: [true, "Rate is required"]
  }
});

const Chart = mongoose.model("Chart", chartSchema);
module.exports = Chart;
