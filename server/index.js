require('dotenv').config();

const
    app = require('express')(),
    http = require('http').createServer(app),
    io = require('socket.io')(http),
    PORT = process.env.PORT || 3000,
    cors = require('cors'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    Chart = require('./api/chart/model/Chart');

mongoose.set('useCreateIndex', true);
mongoose
    .connect(process.env.DATABASE, {useNewUrlParser: true})
    .then(() => {
        console.log('Database is connected');
    })
    .catch(err => {
        console.log({database_error: err});
    });

app.use(cors({credentials: true, origin: 'http://localhost:8080'}));

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.get('/', (req, res) => {
    console.log('test-task');
});

const userRoutes = require('./api/user/route/user');
app.use('/user', userRoutes);

// Websocket
// TODO: move these handlers into some controller
io.on('connection', (socket) => {
    console.log('connection established');

    socket.on('getdata', async () => {
        let chartData = await Chart.find();
        socket.emit('newdata', JSON.stringify(chartData));
    })
});

http.listen(PORT, () => {
    console.log(`App is running on ${PORT}`);
});
